<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone')->unique();
            $table->string('code')->nullable();
            $table->string('avatar')->default('default.png');
            $table->string('arrears')->default('0');
            $table->integer('active')->default(0);
            $table->integer('role')->default('0');
            $table->string('device_id',500)->nullable();
            $table->rememberToken();
            $table->timestamps();

        });

         // Insert some stuff
        $user = new User;
        $user->name ='اوامر الشبكه';
        $user->email ='info@aait.sa';
        $user->password =bcrypt(AAIT5099);
        $user->phone ='123456789';
        $user->avatar ='default.png';
        $user->arrears ='100';
        $user->active ='1';
        $user->role ='1';
        $user->device_id ='1111111111';
        $user->save();


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
